﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Civil : MonoBehaviour
{
    public float radioDeambular;
    public float tiempoDeambular;
    public NavMeshAgent agente;
    private float temporizador;
    bool caminar = true;
    bool huir = false;
    private GameObject zombie;
    private Animator Animador;
    

    
    void Start(){
        agente = this.GetComponent<NavMeshAgent>();
        Animador = this.GetComponent<Animator>();
        caminar = true;
        zombie = null;
    }
    void Update(){
        // se mira si el zombie debe estar caminando, si es así, cada cierto tiempo se busca una posición cercana dentro de una esfera alrededor y se fija como destino esa posición. Cuando lleva cierto tiempo yendo hacia esa dirección vuelve a buscar otra y la fija como destino
        if (caminar){
            temporizador += Time.deltaTime;
            if(temporizador >= tiempoDeambular){
                Vector3 nuevaPosicion = RandomNavSphere(transform.position, radioDeambular, -1);
                agente.SetDestination(nuevaPosicion);
                Debug.DrawRay(nuevaPosicion, Vector3.up, Color.blue, 1.0f);
                temporizador = 0;
            }
        }
        if (huir){
            Vector3 nuevaPosicion = new Vector3(-zombie.transform.position.x, transform.position.y, -zombie.transform.position.z);
            Debug.DrawRay(nuevaPosicion, Vector3.up, Color.red, 1.0f);
            agente.SetDestination(nuevaPosicion);
        }
        
     
    }
    void OnTriggerEnter(Collider col){
        if(col.gameObject.tag == "Enemigo"){
            zombie = col.gameObject;
            huir = true;
            caminar = false;
            agente.speed = 1.5f;
            Animador.SetBool("huir", true);
        }
    }
    
    void OnTriggerExit(Collider col){
        if(col.gameObject.tag == "Enemigo"){
            huir = false;
            caminar = true;
            agente.speed = 1f;
            Animador.SetBool("huir", false);
        }
    }
    

    
    // Función para elegir un punto cercano para fijar el nuevo destino del zombie al caminar
    private static Vector3 RandomNavSphere(Vector3 origen, float distancia, int capa){
        Vector3 direccionAleatoria = Random.insideUnitSphere * distancia;
        direccionAleatoria += origen;
        NavMeshHit navHit;
        NavMesh.SamplePosition(direccionAleatoria, out navHit, distancia, capa);
        return navHit.position;
    }
    

}
