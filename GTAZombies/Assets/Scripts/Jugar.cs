﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Jugar : MonoBehaviour
{
    public void NuevoJuego()
    {
        SceneManager.LoadScene("Pantalla_Juego");   
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");   
    }
    public void Salir()
    {
        Application.Quit();
    }
}
