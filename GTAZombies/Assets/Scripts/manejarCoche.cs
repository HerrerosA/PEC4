﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine.AI;

public class manejarCoche : MonoBehaviour
{
    public Transform[] puntosControl;
    private NavMeshAgent agente;
    private bool ManejaJugador = false;
    private float tiempoEntrarCoche = 1.0f;
    private float tiempoTranscurrido = 0;
    private GameObject jugador;
    private int siguientePuntoControl = 0;

    
    void Start(){
        jugador = GameObject.Find("Jugador");
        agente = this.transform.parent.GetComponent<NavMeshAgent>();
    }
    void Update(){
        if (!ManejaJugador){
            if (!agente.pathPending){
                agente.SetDestination(puntosControl[siguientePuntoControl].position);
                if(agente.remainingDistance != Mathf.Infinity && agente.remainingDistance <= agente.stoppingDistance)
                {
                    siguientePuntoControl = (siguientePuntoControl + 1) % puntosControl.Length;
                    
                }
            }
        }
        if (ManejaJugador){
            siguientePuntoControl = 0;
            tiempoTranscurrido += Time.deltaTime;
            jugador.transform.position = new Vector3(this.transform.position.x, 0.1f, this.transform.position.z);
            if (Input.GetKeyDown("f") && tiempoEntrarCoche < tiempoTranscurrido){
                ControlarAutomatico();
            }
        }

    }
    
    void OnTriggerEnter(Collider col){
        if (col.gameObject.tag == "Player"){
            col.gameObject.GetComponent<Jugador>().AlcanceCoche(this);
        }
    }
    void OnTriggerExit(Collider col){
        if (col.gameObject.tag == "Player"){
            col.gameObject.GetComponent<Jugador>().NoAclanceCoche();
        }
    }
    public void ControlarAutomatico(){
        this.transform.parent.GetComponent<CarUserControl>().enabled = false;
        this.transform.parent.GetComponent<Rigidbody>().isKinematic = true;
        jugador.gameObject.GetComponent<Jugador>().enabled = true;
        Vector3 posicion = new Vector3(transform.position.x - 0.5f, 0.1f, transform.position.z + 3f);
        jugador.gameObject.GetComponent<Jugador>().SalirCoche(posicion);
        agente.enabled = true;
        ManejaJugador = false;
    }
    public void ControlarPorJugador(){
        tiempoTranscurrido = 0;
        this.transform.parent.GetComponent<CarUserControl>().enabled = true;
        this.transform.parent.GetComponent<Rigidbody>().isKinematic = false;
        jugador.transform.position = new Vector3(this.transform.position.x, 0.1f, this.transform.position.z);
        jugador.transform.tag = "PlayerCar";
        agente.enabled = false;
        ManejaJugador = true;
    }
}
