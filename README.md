# 3D - PRACTICA

## CÓMO JUGAR
Para jugar a este juego hay que iniciarlo en la escena Menu.
Una vez ahí está la opción de Jugar o de Salir. Si se selecciona salir se cerrará el juego. 
Si se selecciona Jugar:
El jugador empieza en una posición de la pantalla y se le puede ver en tercera persona. Para moverse se usarán los siguientes controles:
- tecla W para ir hacia adelante, hacia donde enfoca la cámara
- tecla A para girar a la izqueirda con respecto a la cámara
- tecla D para girar a la derecha con respecto a la cámara
- tecla S para dar media vuelta con respecto a la cámara
- C para agacharse
- Barra espaciadora para saltar
- Movimiento del ratón para girar la cámara
- Botón izquierdo del ratón para disparar
Cuando el jugador se encuentre situado cerca de un vehículo rojo podrá conducirlo:
- tecla F para entrar/salir del coche
- tecla W para acelerar, hacia donde enfoca el vehículo y sus ruedas
- tecla A para girar las ruedas a la izquierda
- tecla D para girar las ruedas a la derecha
- tecla S para dar marcha atrás
- Movimiento del ratón para girar la cámara


Una vez iniciado el juego se puede ver en pantalla la siguiente información:
- cantidad de vida del jugador: situada en la parte superior izquierda con un icono de corazón y contada en porcentaje.
- munición: situada arriba a la derecha con el icono de una bala donde se muestra el número de balas disponibles.

Cuando un zombie golpea con su puño directamente al jugador le restará un 10% de la vida y si efectúa un disparo se le restará una bala al total.
Se podrá reabastecer de vida y de munición recogiendo alguno de los objetos disponibles por todo el escenario:
- botiquín: sumará 10% al total de vida del jugador
- munición: sumará 10 balas a la munición disponible

El objetivo del juego es intentar sobrevivir al máximo sin morir. Los zombies van apareciendo de manera infinta en lugares aleatorios de la pantalla, van deambulando y cuando se encuentran cerca del jugador le persiguen.
Los peatones huyen de los zombies y los coches circulan de manera autónoma. El jugador se puede subir en ellos.

Una vez se termina el juego porque el jugador se queda sin vida se muestra la pantalla de Game Over y se puede reiniciar una partida pulsando la tecla Enter.



## ESTRUCTURA E IMPLEMENTACIÓN

# Menú
Se ha creado una pantalla de menú que permite iniciar o salir del juego.

# Jugador
El jugador es una modificación del prefab ThirdPersonController al que se le han cambiado todas las animaciones de movimiento para que pueda llevar siempre la pistola en la mano sin que se vea una postura extraña.
Además dispone de un Rigidbody y un Capsule Collider para que su movimiento resulte lo más adecuado posible. También tiene su correspondiente script de personaje en tercera persona donde se puede definir su velocidad, potencia de salto y otros parámetros. Y un Script de Jugador desde el que se define la cantidad de vidas y de munición (que se mostrarán en pantalla), el alcance de su disparo, el sonido que se emite al disparar e internamente controla todas las acciones que efectúa el jugador.
Al iniciarse se coge el animador del jugador para poder modificar su estado cuando es necesario y se define un tiempo mínimo entre disparos.
Cada vez que es pulsado el botón del ratón se mira si ha transcurrido el tiempo mínimo y si es así se ejecuta la función de disparar.
Esta función lo que hace es restar una bala, ejecutar la animación del disparo y comprobar si en la trayectoria de lo que sería la bala se encuentra un enemigo, de ser así se ejecutará la función del script del enemigo de recibir impacto. Se actualiza la cantidad de balas y se emite el sonido de disparo con un cierto retraso para que no vaya tan adelantado.
La función de morir lo que hace es reproducir la animación de morir.
La función de recibir impacto lo que hace es que resta la cantidad de vida correspondiente y la muestra en pantalla y ejecuta la animación de recibir impacto.
La función que actualiza la animación lo que hace es activar una serie de triggers que son los que tiene el Animator Controller para cambiar de estado.
Si el jugador muere se mira el tiempo transcurrido desde entonces para que se pueda ver la animación de morir entera antes de cambiar a la escena de fin de juego.

# Enemigo
Los enemigos en este caso son un avatar de zombie con un Animator Controller, su correspondiente Rigidbody y Capsule Collider así como un Nav Mesh Agent que le permite deambular por todo el terreno caminable disponible. Además tiene un collider en forma de esfera en la mano derecha que será con la que trate de golpear al jugador.
También tiene un script que controla sus movimientos y acciones.
Al iniciarse se establece el agente que será el que se mueva por la pantalla y el animador y se pone a caminar directamente. Para cada frame se mirará si debe seguir caminando y se sumará tiempo a un contador, si ese contador llega al tiempod definido en el que debe buscar un nuevo destino así lo hará. Ese nuevo destino se busca de manera aleatoria con una esfera alrededor suyo con un radio determinado.
Si detecta que el jugador ha entrado en su trigger del Collider entonces lo perseguirá y esta vez su destino no se fijará de manera aleatoria sino que será la posición del jugador en ese momento. Cuando esté lo suficientemente cerca del jugador entonces procederá a atacarle cada cierto tiempo mientras se encuentre al alcance, ejecutando la animación de ataque y comprobando si una esfera que se simula en la mano derecha ha golpeado al jugador o no.
Si el zombie recibe un impacto de una bala disparada por el jugador se ejecutará su función mencionada anteriormente en la que se generará un sistema de partículas y se pondrá en marcha la animación de recibir disparo. Tras un tiempo determinado el sistema de partículas se destruirá para que no se queden indefinidamente en la pantalla.
Cuando el zombie recibe 4 disparos (ya que cada uno de ellos le quita el 25% de su vida) el zombie dejará de caminar y morirá, ejecutándose su animación de morir y reapareciendo en otro punto del mapa tras un tiempo determinado.

# Objetos recolectables
Existen dos tipos de objetos recolectables que comparten el mismo script, además los dos disponen de un sistema de partículas que giran a su alrededor lo que los permite ser más visibles desde lejos. Cuando el jugador entra en su collider que es un trigger, el objeto desaparece y se le suma al jugador la cantidad correspondiente.

# Coches autónomos
Estos vehículos son del prefab del coche del Standard Assets con un script que permite que se mueva por los waypoints definidos previamente o que sea controlado por el jugador cuando se encuentra dentro del coche.

# Peatones
Hay tres peatones diferentes, que tienen un Animator Controller (diferente para hombre y para mujer para que el movimiento sea ligeramente diferente), tiene su RigidBody y Capsule Collider, así como el Nav Mesh Agent que les permite deambular por todo el terreno caminable.
Además, si detectan que un zombie esstá en su trigger del Collider entonces saldrán corriendo en otra dirección para alejarse de este, una vez se encuentren lo suficientemente lejos volverán a deambular.

# Elementos en pantalla
Los elementos permanentes en la pantalla se actualizan mediante el script del jugador según es necesario.

# Fin del juego
Cuando el jugador se queda sin vida se carga la escena de Game Over y se da la opción de volver a cargar la pantalla de juego pulsando Enter.